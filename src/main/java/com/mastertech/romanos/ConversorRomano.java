package com.mastertech.romanos;

public class ConversorRomano {

    private String[] romanos = {"L", "XL", "X", "IX", "V", "IV", "I"};
    private int[] naturais = {50, 40, 10, 9, 5, 4, 1};

    public String converter(int num) {
        String result = "";

        if(num > 50) {
            throw new RuntimeException("Número deve ser menor ou igual a 50");
        }

        int i = 0;

        while (i < romanos.length) {
            if (num >= naturais[i]) {
                result += romanos[i];
                System.out.print(romanos[i]);
                num -= naturais[i];
            } else {
                i ++;
            }
        }

        return result;
    }
}
