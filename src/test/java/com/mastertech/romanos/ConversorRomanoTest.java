package com.mastertech.romanos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversorRomanoTest {

    @Test
    public void testaConversaoRomano() {
        Assertions.assertEquals("XLIX", new ConversorRomano().converter(49));
    }

    @Test
    public void testaConversaoRomanoMaiorQueLimite() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            new ConversorRomano().converter(51);
        });
    }
}
